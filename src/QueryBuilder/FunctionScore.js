"use strict";

const _ = require("lodash");
const BaseQueryBuilder = require("./BaseWrapper");

class FunctionScore extends BaseQueryBuilder {
  constructor() {
    super();
    this._dsl = {};
    this._functions = [];
    this._options = undefined;
    return new Proxy(this, this);
  }

  /**
   * Returns prepared DSL
   *
   * @returns {Object}
   */
  getDSL() {
    let dsl = {
      ..._.cloneDeep(this._options),
      query: super.getDSL().query,
    };
    if (this._functions.length) {
      dsl.functions = this._functions;
    }
    return dsl;
  }

  /**
   * @param {String} name
   * @param {*} value
   */
  function(name, value) {
    this._functions.push({
      [name]: value,
    });
    return this;
  }

  /**
   *
   * @param {Object} params
   */
  options(params) {
    this._options = params;
    return this;
  }
}

module.exports = FunctionScore;
