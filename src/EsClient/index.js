"use strict";

/**@typedef {Connection} */
const Connection = require("./Connection");

class EsClient {
  /**
   * Creates a new instance
   *
   * @param {Object} Config
   */
  constructor(Config) {
    this.connections = {};
    const esConfigs = Config.get("elasticsearch");
    for (let connectionName in esConfigs.connections) {
      let config = esConfigs.connections[connectionName];
      this.connections[connectionName] = new Connection(connectionName, config);
    }
    /**@type {Connection} */
    this.defaultConnection = this.connections[esConfigs.connection];
    return new Proxy(this, this);
  }

  /**
   * Magic getter function
   *
   */
  get(target, prop) {
    if (typeof this[prop] !== "undefined") {
      return this[prop];
    }
    if (typeof this.defaultConnection[prop] === "function") {
      return function () {
        return this.defaultConnection[prop](...arguments);
      };
    } else if (typeof this.defaultConnection[prop] !== "undefined") {
      return this.defaultConnection[prop];
    }
  }

  /**
   * This method is used to get connection object
   *
   * @param {String} [name]
   * @returns {Connection}
   */
  connection(name = null) {
    const connection = !name ? this.defaultConnection : this.connections[name];
    if (!connection) {
      throw new Error("Elasticsearch connection doesn't exists");
    }
    if (!connection.indexName) {
      throw new Error("Index name not provided");
    }
    return connection;
  }

  /**
   * This method is used to monitor all connections
   */
  monitorConnections() {
    for (var connectionName in this.connections) {
      this.connections[connectionName].monitorConnection();
    }
  }
}

module.exports = EsClient;
