"use strict";

const _ = require("lodash");
const BaseQueryBuilder = require("./Base");

class BaseWrapper extends BaseQueryBuilder {
  constructor() {
    super();
    if (this.constructor.name === "BaseWrapper") {
      throw Error("BaseWrapper class can't be instantiated");
    }
    this._dsl = {};
    this._sort = [];
    this._limit = null;
    this._offset = null;
    this._scriptFields = {};
  }

  /**
   * Returns prepared DSL
   *
   * @returns {Object}
   */
  getDSL() {
    let dsl = super.getDSL();
    if (typeof this._limit === "number" && !isNaN(this._limit)) {
      dsl.size = this._limit;
    }
    if (typeof this._offset === "number" && !isNaN(this._offset)) {
      dsl.from = this._offset;
    }
    if (this._sort.length) {
      if (this._sort.length === 1) {
        dsl.sort = this._sort[0];
      } else {
        dsl.sort = _.cloneDeep(this._sort);
      }
    }
    if (!_.isEmpty(this._scriptFields)) {
      dsl.script_fields = _.cloneDeep(this._scriptFields);
    }
    return dsl;
  }

  /**
   * This method is used to add limit to query result
   *
   * @param {Number} size
   * @returns {BaseWrapper}
   */
  limit(size) {
    this._limit = size;
    return this;
  }

  /**
   * This method is used to add offset to query result
   *
   * @param {Number} from
   * @returns {BaseWrapper}
   */
  offset(from) {
    this._offset = from;
    return this;
  }

  /**
   * This method is used to apply sort criteria
   *
   * @param {String} field
   * @param {String} [condition='asc']
   */
  sort(field, condition = "asc") {
    this._sort.push({ [field]: condition });
    return this;
  }

  /**
   * This method is used to add script field
   *
   * @param {String} field
   * @param {Object|String} script
   * @returns {BaseWrapper}
   */
  scriptField(field, script) {
    this._scriptFields[field] = { script };
    return this;
  }
}

module.exports = BaseWrapper;
