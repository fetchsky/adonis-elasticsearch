"use strict";
const moment = use("moment");

class Timer {
  constructor() {
    this.startTime = null;
    this.endTime = null;
  }
  start() {
    this.startTime = moment();
    this.endTime = null;
  }
  stop() {
    this.endTime = moment();
  }
  getExecutionTime(raw = false) {
    if (!this.startTime) {
      return false;
    }
    if (!this.endTime) {
      this.stop();
    }
    var time = moment.duration(this.endTime.diff(this.startTime)).asMilliseconds();
    return raw ? time : time + " ms";
  }
}

module.exports = Timer;
