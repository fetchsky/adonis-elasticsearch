"use strict";

const _ = require("lodash");

class FilterQuery {
  constructor() {
    this._hasChild = [];
    this._hasParent = [];
    this._whereConditions = [];
  }

  /**
   * This method is used to get where conditions
   *
   * @returns {Array}
   */
  get conditions() {
    return this._whereConditions;
  }

  /**
   * This method is used to add multiple where conditions to dsl
   *
   * @param {Array<Object>} conditions
   * @returns {FilterQuery}
   */
  whereAll(conditions) {
    if (!_.isEmpty(conditions)) {
      conditions.forEach((condition) => {
        this.where(condition);
      });
    }
    return this;
  }

  /**
   * This method is used to add where condition to dsl
   *
   * @param {Object} condition
   * @returns {FilterQuery}
   */
  where(condition) {
    this._whereConditions.push(condition);
    return this;
  }

  /**
   * This method is used to add match condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @returns {FilterQuery}
   */
  match(attribute, value) {
    return this.where({
      match: {
        [attribute]: value,
      },
    });
  }

  /**
   * This method is used to add match all condition to dsl
   *
   * @returns {FilterQuery}
   */
  matchAll() {
    return this.where({
      match_all: {},
    });
  }

  /**
   * This method is used to add term condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @returns {FilterQuery}
   */
  term(attribute, value) {
    return this.where({
      term: {
        [attribute]: value,
      },
    });
  }

  /**
   * This method is used to add terms condition to dsl
   *
   * @param {String} attribute
   * @param {Array<String|Boolean|Number>} values
   * @returns {FilterQuery}
   */
  terms(attribute, values) {
    return this.where({
      terms: {
        [attribute]: values,
      },
    });
  }

  /**
   * This method is used to add exists condition to dsl
   *
   * @param {String} attribute
   * @returns {FilterQuery}
   */
  exists(attribute) {
    return this.where({
      exists: {
        field: attribute,
      },
    });
  }

  /**
   * This method is used to add bool to dsl
   *
   * @param {boolFunction} boolFunction
   * @returns {FilterQuery}
   */
  bool(boolFunction) {
    const BaseQueryBuilder = require("./Base");
    let baseQuery = new BaseQueryBuilder();
    if (typeof boolFunction === "function") {
      boolFunction(baseQuery);
    }
    let baseDsl = baseQuery.getDSL();
    return this.where(baseDsl.query);
  }

  /**
   * This method is used to add range (greater than) condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @param {Boolean} [allowEqual=false]
   * @returns {FilterQuery}
   */
  greaterThan(attribute, value, allowEqual = false) {
    return this.where({
      range: {
        [attribute]: {
          [allowEqual ? "gte" : "gt"]: value,
        },
      },
    });
  }

  /**
   * This method is used to add range (greater than or not exists) condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @param {Boolean} [allowEqual=false]
   * @returns {FilterQuery}
   */
  greaterThanOrNotExists(attribute, value, allowEqual = false) {
    return this.bool((boolQuery) => {
      boolQuery
        .should((shouldQuery) => {
          shouldQuery
            .bool((shouldBoolQuery) => {
              shouldBoolQuery.mustNot((mustNotQuery) => {
                mustNotQuery.exists(attribute);
              });
            })
            .greaterThan(attribute, value, allowEqual);
        })
        .minimumShouldMatch(1);
    });
  }

  /**
   * This method is used to add range (less than) condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @param {Boolean} [allowEqual=false]
   * @returns {FilterQuery}
   */
  lessThan(attribute, value, allowEqual = false) {
    return this.where({
      range: {
        [attribute]: {
          [allowEqual ? "lte" : "lt"]: value,
        },
      },
    });
  }

  /**
   * This method is used to add range (less than or not exists) condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @param {Boolean} [allowEqual=false]
   * @returns {FilterQuery}
   */
  lessThanOrNotExists(attribute, value, allowEqual = false) {
    return this.bool((boolQuery) => {
      boolQuery
        .should((shouldQuery) => {
          shouldQuery
            .bool((shouldBoolQuery) => {
              shouldBoolQuery.mustNot((mustNotQuery) => {
                mustNotQuery.exists(attribute);
              });
            })
            .lessThan(attribute, value, allowEqual);
        })
        .minimumShouldMatch(1);
    });
  }

  /**
   * This method is used to add has nested path condition to dsl
   *
   * @param {String} nestedPath
   * @param {Function} queryFunction
   * @returns {FilterQuery}
   */
  nested(nestedPath, queryFunction) {
    const JoinQueryBuilder = require("./Join");
    let nestedQuery = new JoinQueryBuilder();
    if (typeof queryFunction === "function") {
      queryFunction(nestedQuery);
    }
    let nestedDSL = nestedQuery.getDSL();
    return this.where({
      nested: {
        ...nestedQuery.getOptions(),
        path: nestedPath,
        query: nestedDSL.query,
        inner_hits: nestedDSL.inner_hits,
      },
    });
  }

  /**
   * This method is used to add has child condition to dsl
   *
   * @param {String} doc
   * @param {Function} queryFunction
   * @param {Object} [options={}]
   * @returns {FilterQuery}
   */
  hasChild(doc, queryFunction) {
    const JoinQueryBuilder = require("./Join");
    let childQuery = new JoinQueryBuilder();
    if (typeof queryFunction === "function") {
      queryFunction(childQuery);
    }
    let childDSL = childQuery.getDSL();
    return this.where({
      has_child: {
        ...childQuery.getOptions(),
        type: doc,
        query: childDSL.query,
        inner_hits: childDSL.inner_hits,
      },
    });
  }

  /**
   * This method is used to add has parent condition to dsl
   *
   * @param {String} doc
   * @param {Function} queryFunction
   * @returns {FilterQuery}
   */
  hasParent(doc, queryFunction) {
    const JoinQueryBuilder = require("./Join");
    let parentQuery = new JoinQueryBuilder();
    if (typeof queryFunction === "function") {
      queryFunction(parentQuery);
    }
    let parentDSL = parentQuery.getDSL();
    return this.where({
      has_parent: {
        ...parentQuery.getOptions(),
        type: doc,
        query: parentDSL.query,
        inner_hits: parentDSL.inner_hits,
      },
    });
  }

  /**
   * This method is used to add wildcard condition to dsl
   *
   * @param {String} attribute
   * @param {String|Object} value
   * @returns {FilterQuery}
   */
  wildcard(attribute, value) {
    return this.where({
      wildcard: {
        [attribute]: value,
      },
    });
  }

  /**
   * This method is used to add script condition to dsl
   *
   * @param {String} script
   * @param {Object} [options]
   * @param {Object} [options.params]
   * @param {String} [options.lang]
   * @returns {FilterQuery}
   */
  script(script, options) {
    return this.where({
      script: {
        script: {
          ...options,
          source: script,
        },
      },
    });
  }

  /**
   * This method is used to add filter query
   *
   * @param {queryFunction} queryFunction
   * @returns {FilterQuery}
   */
  filter(queryFunction) {
    if (typeof queryFunction === "function") {
      this.bool((boolQuery) => {
        boolQuery.should(queryFunction);
      });
    }
    return this;
  }

  /**
   * This method is used to add should query
   *
   * @param {queryFunction} queryFunction
   * @returns {FilterQuery}
   */
  should(queryFunction) {
    if (typeof queryFunction === "function") {
      this.bool((boolQuery) => {
        boolQuery.should(queryFunction);
      });
    }
    return this;
  }

  /**
   * This method is used to set minimum should match property
   *
   * @param {Number} minimum
   * @returns {FilterQuery}
   */
  minimumShouldMatch(minimum) {
    this._minimumShouldMatch = minimum;
    return this;
  }

  /**
   * This method is used to add must query
   *
   * @param {queryFunction} queryFunction
   * @returns {FilterQuery}
   */
  must(queryFunction) {
    if (typeof queryFunction === "function") {
      this.bool((boolQuery) => {
        boolQuery.mustNot(queryFunction);
      });
    }
    return this;
  }

  /**
   * This method is used to add must_not query
   *
   * @param {queryFunction} queryFunction
   * @returns {FilterQuery}
   */
  mustNot(queryFunction) {
    if (typeof queryFunction === "function") {
      this.bool((boolQuery) => {
        boolQuery.mustNot(queryFunction);
      });
    }
    return this;
  }
}

module.exports = FilterQuery;
