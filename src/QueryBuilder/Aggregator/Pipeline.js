"use strict";

/**@typedef {import('.')} Aggregator */
const _ = require("lodash");

class PipelineAggregator {
  /**
   * Returns those aggregations which accepts :
   *  - buckets path as string
   *  - object for extra properties (Optional)
   */
  get typeA() {
    return [
      "movingFn",
      "avgBucket",
      "derivative",
      "maxBucket",
      "minBucket",
      "sumBucket",
      "movingAvg",
      "serialDiff",
      "statsBucket",
      "cumulativeSum",
      "percentilesBucket",
      "extendedStatsBucket"
    ];
  }

  /**
   * Returns those aggregations which accepts :
   *  - buckets path as object
   *  - script as object
   *  - object for extra properties (Optional)
   */
  get typeB() {
    return ["bucketScript", "bucketSelector"];
  }

  /**
   * Returns those aggregations which methods are explicitly defined
   *
   */
  get typeC() {
    return ["bucketSort"];
  }

  /**
   * @param {Aggregator} Aggregator
   */
  constructor(Aggregator) {
    this._Aggregator = Aggregator;
    return new Proxy(this, this);
  }

  /**
   * Magic getter function for metrics aggregation
   */
  get(target, prop) {
    let self = this;
    let agg = _.snakeCase(prop);
    if (this.typeA.indexOf(prop) > -1) {
      return function() {
        return self._applyATypeAggregation(agg, ...arguments);
      };
    } else if (this.typeB.indexOf(prop) > -1) {
      return function() {
        return self._applyBTypeAggregation(agg, ...arguments);
      };
    }
    return this[prop];
  }

  /**
   * This method is used to apply bucket sort aggregation
   *
   * @param {String} name
   * @param {Array|Object} sort
   * @param {Object} [extra]
   * @returns {Aggregator}
   */
  bucketSort(name, sort, extra) {
    return this._Aggregator.aggs(name, {
      bucket_sort: { sort, ..._.omit(extra, "aggs") }
    });
  }

  /**
   * This method is used to apply A type pipeline aggregation
   *
   * @param {String} name
   * @param {String} bucketsPath
   * @param {Object} [extra]
   * @returns {Aggregator}
   */
  _applyATypeAggregation(agg, name, bucketsPath, extra) {
    if (typeof extra === "function") {
      return this._Aggregator.aggs(
        name,
        {
          [agg]: { buckets_path: bucketsPath }
        },
        extra
      );
    }
    return this._Aggregator.aggs(name, {
      [agg]: { buckets_path: bucketsPath, ..._.omit(extra, "aggs") }
    });
  }

  /**
   * This method is used to apply B type pipeline aggregation
   *
   * @param {String} name
   * @param {Object} bucketsPath
   * @param {Object} script
   * @param {Object} [extra]
   * @returns {Aggregator}
   */
  _applyBTypeAggregation(agg, name, bucketsPath, script, extra) {
    if (typeof extra === "function") {
      return this._Aggregator.aggs(
        name,
        {
          [agg]: { buckets_path: bucketsPath, script }
        },
        extra
      );
    }
    return this._Aggregator.aggs(name, {
      [agg]: { buckets_path: bucketsPath, script, ..._.omit(extra, "aggs") }
    });
  }
}

module.exports = PipelineAggregator;
