"use strict";

/**@typedef {import('.')} Aggregator */
const _ = require("lodash");

class MatrixAggregator {
  /**
   * @param {Aggregator} Aggregator
   */
  constructor(Aggregator) {
    this._Aggregator = Aggregator;
  }

  /**
   * This method is used to apply bucket sort aggregation
   *
   * @param {String} name
   * @param {Array} fields
   * @returns {Aggregator}
   */
  matrixStats(name, fields) {
    return this._Aggregator.aggs(name, {
      matrix_stats: { fields }
    });
  }
}

module.exports = MatrixAggregator;
