"use strict";

/**
 * @callback queryFunction
 * @param {FilterQuery} filterQuery
 */

const _ = require("lodash");
const FilterQuery = require("./FilterQuery");

class BaseQueryBuilder {
  constructor() {
    this._dsl = {};
    this._boost = undefined;
    this._function_score = undefined;
    this._mustConditions = new FilterQuery();
    this._shouldConditions = new FilterQuery();
    this._filterConditions = new FilterQuery();
    this._mustNotConditions = new FilterQuery();
  }

  /**
   * Returns prepared DSL
   *
   * @returns {Object}
   */
  getDSL() {
    let dsl = {
      query: {},
    };
    if (this._mustConditions.conditions.length) {
      _.set(
        dsl,
        "query.bool.must",
        _.cloneDeep(this._mustConditions.conditions)
      );
    }
    if (this._mustNotConditions.conditions.length) {
      _.set(
        dsl,
        "query.bool.must_not",
        _.cloneDeep(this._mustNotConditions.conditions)
      );
    }
    if (this._filterConditions.conditions.length) {
      _.set(
        dsl,
        "query.bool.filter",
        _.cloneDeep(this._filterConditions.conditions)
      );
    }
    if (this._shouldConditions.conditions.length) {
      _.set(
        dsl,
        "query.bool.should",
        _.cloneDeep(this._shouldConditions.conditions)
      );
      if (!isNaN(this._shouldConditions._minimumShouldMatch)) {
        dsl.query.bool.minimum_should_match = this._shouldConditions._minimumShouldMatch;
      }
    }
    if (_.isEmpty(dsl.query.bool)) {
      dsl.query = {
        match_all: {},
      };
      if (_.isInteger(this._boost)) {
        dsl.query.match_all.boost = this._boost;
      }
    } else if (_.isInteger(this._boost)) {
      dsl.query.bool.boost = this._boost;
    }
    if (this._function_score) {
      let functionScore = _.cloneDeep(this._function_score);
      if (functionScore.query.match_all) {
        functionScore.query = {};
      }
      _.merge(functionScore.query, dsl.query);
      return {
        query: {
          function_score: functionScore,
        },
      };
    }
    return dsl;
  }

  /**
   *
   * @param {Function} queryFunction
   * @returns {FilterQuery}
   */
  functionScore(queryFunction) {
    const FunctionScore = require("./FunctionScore");
    let functionScoreQuery = new FunctionScore();
    if (typeof queryFunction === "function") {
      queryFunction(functionScoreQuery);
    }
    this._function_score = functionScoreQuery.getDSL();
    return this;
  }

  /**
   * This method is used to get must query
   *
   * @param {queryFunction} queryFunction
   * @returns {BaseQueryBuilder}
   */
  must(queryFunction) {
    if (typeof queryFunction === "function") {
      queryFunction(this._mustConditions);
    }
    return this;
  }

  /**
   * This method is used to get must_not query
   *
   * @param {queryFunction} queryFunction
   * @returns {BaseQueryBuilder}
   */
  mustNot(queryFunction) {
    if (typeof queryFunction === "function") {
      queryFunction(this._mustNotConditions);
    }
    return this;
  }

  /**
   * This method is used to get filter query
   *
   * @param {queryFunction} queryFunction
   * @returns {BaseQueryBuilder}
   */
  filter(queryFunction) {
    if (typeof queryFunction === "function") {
      queryFunction(this._filterConditions);
    }
    return this;
  }

  /**
   * This method is used to get should query
   *
   * @param {queryFunction} queryFunction
   * @returns {BaseQueryBuilder}
   */
  should(queryFunction) {
    if (typeof queryFunction === "function") {
      queryFunction(this._shouldConditions);
    }
    return this;
  }

  /**
   * This method is used to set minimum should match property
   *
   * @param {Number} minimum
   * @returns {BaseQueryBuilder}
   */
  minimumShouldMatch(minimum) {
    this._shouldConditions.minimumShouldMatch(minimum);
    return this;
  }

  /**
   * This method is used to set boost value for query
   */
  boost(factor) {
    this._boost = factor;
  }

  /**
   * This method is used to combine external bool conditions to current query
   *
   * @param {Object} externalQuery
   * @returns {BaseQueryBuilder}
   */
  combineQuery(externalQuery) {
    if (!externalQuery) {
      return this;
    }
    const EsQueryBuilder = require("./index");
    let external = {};
    if (externalQuery instanceof EsQueryBuilder) {
      externalQuery.filter((externalFilterQuery) => {
        external.filter = externalFilterQuery.conditions;
      });
      externalQuery.should((externalShouldQuery) => {
        external.should = externalShouldQuery.conditions;
        external.minimum_should_match = externalShouldQuery._minimumShouldMatch;
      });
      externalQuery.must((externalMustQuery) => {
        external.must = externalMustQuery.conditions;
      });
      externalQuery.mustNot((externalMustNotQuery) => {
        external.must_not = externalMustNotQuery.conditions;
      });
      external.boost = externalQuery._boost;
      externalQuery = external;
    }
    if (!_.isEmpty(externalQuery)) {
      this.filter((filterQuery) => {
        if (_.isArray(externalQuery.filter)) {
          filterQuery.whereAll(externalQuery.filter);
        } else if (externalQuery.filter) {
          filterQuery.where(externalQuery.filter);
        }
      })
        .should((shouldQuery) => {
          if (_.isArray(externalQuery.should)) {
            shouldQuery.whereAll(externalQuery.should);
          } else if (externalQuery.should) {
            shouldQuery.where(externalQuery.should);
          }
        })
        .must((mustQuery) => {
          if (_.isArray(externalQuery.must)) {
            mustQuery.whereAll(externalQuery.must);
          } else if (externalQuery.must) {
            mustQuery.where(externalQuery.must);
          }
        })
        .mustNot((mustNotQuery) => {
          if (_.isArray(externalQuery.must_not)) {
            mustNotQuery.whereAll(externalQuery.must_not);
          } else if (externalQuery.must_not) {
            mustNotQuery.where(externalQuery.must_not);
          }
        })
        .minimumShouldMatch(externalQuery.minimum_should_match)
        .boost(externalQuery.boost);
    }
    return this;
  }
}

module.exports = BaseQueryBuilder;
