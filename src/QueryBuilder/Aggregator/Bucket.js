"use strict";

/**@typedef {import('.')} Aggregator */
const _ = require("lodash");

class BucketAggregator {
  /**
   * Returns those aggregations which accepts :
   *  - field as string
   *  - object for extra properties (Optional)
   */
  get typeA() {
    return [
      "autoDateHistogram",
      "dateHistogram",
      "dateRange",
      "diversifiedSampler",
      "geoDistance",
      "geohashGrid",
      "histogram",
      "ipRange",
      "missing",
      "significantText",
      "significantTerms"
    ];
  }

  /**
   * Returns those aggregations which accepts :
   *  - object as a param
   */
  get typeB() {
    return ["filter", "sampler", "global", "reverseNested"];
  }

  /**
   * Returns those aggregations which methods are explicitly defined
   *
   */
  get typeC() {
    return [
      "adjacencyMatrix",
      "parent",
      "children",
      "nested",
      "composite",
      "filters",
      "range",
      "terms"
    ];
  }

  /**
   * @param {Aggregator} Aggregator
   */
  constructor(Aggregator) {
    this._Aggregator = Aggregator;
    return new Proxy(this, this);
  }

  /**
   * Magic getter function for metrics aggregation
   */
  get(target, prop) {
    let self = this;
    if (this.typeA.indexOf(prop) > -1) {
      return function() {
        let agg = _.snakeCase(prop);
        let name = arguments[0];
        let field = arguments[1];
        let extra = arguments[2];
        let nested = arguments[3];
        if (!field || typeof field !== "string") {
          return self._Aggregator;
        }
        let aggs = { [agg]: { field } };
        if (extra) {
          if (typeof extra === "object") {
            _.merge(aggs[agg], _.omit(extra, ["aggs"]));
          } else if (typeof extra === "function") {
            nested = extra;
          }
        }
        return self._Aggregator.aggs(name, aggs, nested);
      };
    } else if (self.typeB.indexOf(prop) > -1) {
      return function() {
        let agg = _.snakeCase(prop);
        let name = arguments[0];
        let params = arguments[1];
        let nested = arguments[2];
        let aggs = { [agg]: { ...params } };
        return self._Aggregator.aggs(name, aggs, nested);
      };
    }
    return this[prop];
  }

  /**
   * This method is used to apply adjacency matrix
   *
   * @param {String} name
   * @param {Object} filters
   * @param {Function} nested
   * @returns {Aggregator}
   */
  adjacencyMatrix(name, filters, nested) {
    return this._Aggregator.aggs(
      name,
      {
        adjacency_matrix: { filters }
      },
      nested
    );
  }

  /**
   * This method is used to apply parent aggregation
   *
   * @param {String} name
   * @param {String} doc
   * @param {Function} nested
   * @returns {Aggregator}
   */
  parent(name, doc, nested) {
    return this._Aggregator.aggs(
      name,
      {
        parent: { type: doc }
      },
      nested
    );
  }

  /**
   * This method is used to apply children aggregation
   *
   * @param {String} name
   * @param {String} doc
   * @param {Function} nested
   * @returns {Aggregator}
   */
  children(name, doc, nested) {
    return this._Aggregator.aggs(
      name,
      {
        children: { type: doc }
      },
      nested
    );
  }

  /**
   * This method is used to apply nested aggregation
   *
   * @param {String} name
   * @param {String} nestedPath
   * @param {Function} nested
   * @returns {Aggregator}
   */
  nested(name, nestedPath, nested) {
    return this._Aggregator.aggs(
      name,
      {
        nested: { path: nestedPath }
      },
      nested
    );
  }

  /**
   * This method is used to apply composite aggregation
   *
   * @param {String} name
   * @param {Array} sources
   * @param {Object} [extra]
   * @param {Function} nested
   * @returns {Aggregator}
   */
  composite(name, sources, extra, nested) {
    if (typeof extra === "function") {
      return this._Aggregator.aggs(
        name,
        {
          composite: { sources }
        },
        extra
      );
    }
    return this._Aggregator.aggs(
      name,
      {
        composite: { sources, ..._.omit(extra, "aggs") }
      },
      nested
    );
  }

  /**
   * This method is used to apply filters aggregation
   *
   * @param {String} name
   * @param {Object} filters
   * @param {Object} [extra]
   * @param {Function} nested
   * @returns {Aggregator}
   */
  filters(name, filters, extra, nested) {
    if (typeof extra === "function") {
      return this._Aggregator.aggs(
        name,
        {
          filters: { filters }
        },
        extra
      );
    } else {
      return this._Aggregator.aggs(
        name,
        {
          filters: { filters, ..._.omit(extra, "aggs") }
        },
        nested
      );
    }
  }

  /**
   * This method is used to apply filters aggregation
   *
   * @param {String} name
   * @param {String|Object} field
   * @param {Array} ranges
   * @param {Object} [extra]
   * @param {Function} nested
   * @returns {Aggregator}
   */
  range(name, field, ranges, extra, nested) {
    let aggs = { range: { ranges } };
    if (field && typeof field === "string") {
      aggs.range.field = field;
    } else if (field && typeof field === "object" && field.script) {
      aggs.range.script = field.script;
    } else {
      return this._Aggregator;
    }
    if (typeof extra === "function") {
      return this._Aggregator.aggs(name, aggs, extra);
    } else {
      _.merge(aggs.range, _.omit(extra, ["aggs"]));
      return this._Aggregator.aggs(name, aggs, nested);
    }
  }

  /**
   * This method is used to apply filters aggregation
   *
   * @param {String} name
   * @param {String|Object} field
   * @param {Object} [extra]
   * @param {Function} nested
   * @returns {Aggregator}
   */
  terms(name, field, extra, nested) {
    let aggs = { terms: {} };
    if (field && typeof field === "string") {
      aggs.terms.field = field;
    } else if (field && typeof field === "object" && field.script) {
      aggs.terms.script = field.script;
    } else {
      return this._Aggregator;
    }
    if (typeof extra === "function") {
      return this._Aggregator.aggs(name, aggs, extra);
    } else {
      _.merge(aggs.terms, _.omit(extra, ["aggs"]));
      return this._Aggregator.aggs(name, aggs, nested);
    }
    return this._Aggregator;
  }
}

module.exports = BucketAggregator;
