"use strict";

const _ = require("lodash");
const InnerHit = require("./InnerHit");
const BaseQueryBuilder = require("./BaseWrapper");

class Join extends BaseQueryBuilder {
  constructor() {
    super();
    this._innerHit = null;
    this._options = {};
    return new Proxy(this, this);
  }

  /**
   * Return supported methods
   */
  get SUPPORTED_METHODS() {
    return [
      "getDSL",
      "innerHit",
      "must",
      "filter",
      "should",
      "mustNot",
      "minimumShouldMatch",
    ];
  }

  get(target, prop) {
    let self = this;
    if (typeof this[prop] !== "undefined") {
      return this[prop];
    } else if (typeof this._mustConditions[prop] !== "undefined") {
      return function () {
        return self._mustConditions[prop](...arguments);
      };
    }
  }

  /**
   * This method is used to get prepared dsl
   *
   * @returns {Object}
   */
  getDSL() {
    let dsl = super.getDSL();
    if (this._innerHit) {
      dsl.inner_hits = this._innerHit.getDSL();
    }
    return dsl;
  }

  /**
   * This method is used to options for join query
   *
   * @returns {Object}
   */
  getOptions() {
    return _.cloneDeep(this._options);
  }

  /**
   * This method is used to include/exclude inner hits
   *
   * @param {Boolean} [include=true]
   * @param {Function} [innerHitBuilder]
   */
  innerHit(include, innerHitBuilder) {
    if (!include) {
      this._innerHit = null;
    } else {
      this._innerHit = new InnerHit();
      if (typeof innerHitBuilder === "function") {
        innerHitBuilder(this._innerHit);
      }
    }
    return this;
  }

  /**
   *
   * @param {Object} params
   */
  options(params) {
    this._options = params;
    return this;
  }
}

module.exports = Join;
